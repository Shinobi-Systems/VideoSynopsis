const VideoSynopsis = require('./index.js');
const newEngine = VideoSynopsis.newEngine({
    debugLog: false,
    //build directories
    subtractedBackgroundsDirectory: 'rawSubtractions/',
    mergedFramesDirectory: 'mergedFrames/',
    firstFrameDirectory: 'firstFrame/',
    ffmpegBinary: 'ffmpeg',
});

newEngine.std.on('percent',(percent) => {
    console.log('Completed : ' + percent + '%')
})
newEngine.std.on('stderr',(logData) => {
    console.log(...logData)
})

console.log(newEngine.about());

const runTheTest = async () => {
    await newEngine.run({
        //does videos named 1.mp4 and 2.mp4
        videos: ['1','2'],
        //frame rate
        fps: 25,
        // output location
        output: 'videoOutput.mp4'
    });

    await newEngine.cleanBuildFiles();
}
runTheTest()

// setTimeout(() => {
//     const newEngine2 = VideoSynopsis.newEngine();
//     console.log(newEngine2.about());
// },2000)
