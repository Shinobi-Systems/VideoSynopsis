
# Shinobi Video Synopsis Tool
https://shinobi.video
by Moe Alam (moeiscool), Shinobi Systems

Create Video Synopsis from provided videos. Frames are extracted from each video provided and backgrounds are subtracted around moving elements and merged into a single video for quick review.

Demo : https://www.youtube.com/watch?v=SF-cYNGxgS0

Depends on :
- FFmpeg 3.3 to 3.4 is recommended.
    - `-pattern_type glob` may not work in other versions.
- ImageMagick
- openv4nodejs (Node Module, Installs OpenCV Automatically)
- moment (Node Module)
- gm (Node Module)

**Tested on Ubuntu 19.10 Server**


## Install FFmpeg

```
apt install ffmpeg -y
```

## Install ImageMagick

```
apt install imagemagick -y
```

## Install Shinobi Video Synopsis Tool
```
npm install shinobi-video-synopsis
```

## Example Usage

```
const VideoSynopsis = require('shinobi-video-synopsis');
const newEngine = VideoSynopsis.newEngine({
    debugLog: false,
    //build directories
    subtractedBackgroundsDirectory: 'rawSubtractions/',
    mergedFramesDirectory: 'mergedFrames/',
    firstFrameDirectory: 'firstFrame/',
    ffmpegBinary: 'ffmpeg',
});

newEngine.std.on('percent',(percent) => {
    console.log('Completed : ' + percent + '%')
})
newEngine.std.on('stderr',(logData) => {
    console.log(...logData)
})

console.log(newEngine.about());

const runTheTest = async () => {
    await newEngine.run({
        //does videos named 1.mp4 and 2.mp4
        videos: ['1','2'],
        //frame rate
        fps: 25,
        // output location
        output: 'videoOutput.mp4'
    });

    await newEngine.cleanBuildFiles();
}
runTheTest()
```


Licensed under GPLv3 https://www.gnu.org/licenses/gpl-3.0.en.html
